package com.ken.entity.base.util;


import com.ken.entity.base.protocol.R;

public class ResultUtils {

    /**
     * 返回成功的R对象
     * @param <T>
     * @return
     */
    public static <T> R<T> createSucc(T data){
        return ResultUtils.create(RCode.SUCCESS.code, RCode.SUCCESS.message, data);
    }

    /**
     * 返回失败的R对象
     * @param <T>
     * @return
     */
    public static <T> R<T> createFail(Integer code, String msg){
        return ResultUtils.create(code, msg, null);
    }

    /**
     * 返回失败的R对象
     * @param <T>
     * @return
     */
    public static <T> R<T> createFail(RCode rCode){
        return ResultUtils.create(rCode.getCode(), rCode.getMessage(), null);
    }

    /**
     * 返回失败的R对象
     * @param <T>
     * @return
     */
    public static <T> R<T> createFail(RCode rCode, String msg){
        return ResultUtils.create(rCode.getCode(), msg, null);
    }

    /**
     * 返回其他的result对象
     * @param <T>
     * @return
     */
    public static <T> R<T> create(Integer code, String msg, T data){
        return new R<T>().setCode(code)
                .setMessage(msg)
                .setData(data);
    }

}
