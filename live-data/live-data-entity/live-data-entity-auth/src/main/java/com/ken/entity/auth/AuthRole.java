package com.ken.entity.auth;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ken.entity.base.entity.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 角色表(AuthRole)表实体类
 *
 * @author makejava
 * @since 2021-09-12 11:27:24
 */
@Data
@Accessors(chain = true)
public class AuthRole extends BaseEntity {
    //主键
    @TableId(value = "rid", type = IdType.AUTO)
    private Long id;
    //角色名称
    @TableField(value = "role_name")
    private String roleName;
    //角色别名
    @TableField(value = "role_tag")
    private String roleTag;
}
