package com.ken.common.cache.cluster.guava;

import com.google.common.cache.Cache;
import com.ken.common.cache.handler.MemoryCacheHandler;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * 基于JVM内存的缓存策略
 */
public class GuavaMemoryCacheHandler<K extends Serializable, V> extends MemoryCacheHandler<K, V> {

    @Autowired
    private Cache cache;

    @Override
    public boolean putCache(K key, V value) {
        CacheValue cacheValue = new CacheValue()
                .setValue(value)
                .setCreateTime(System.currentTimeMillis())
                .setTimeout(-1L);
        cache.put(key, cacheValue);
        return true;
    }

    @Override
    public boolean putCache(K key, V value, Long timeOut, TimeUnit unit) {
        CacheValue cacheValue = new CacheValue()
                .setValue(value)
                .setCreateTime(System.currentTimeMillis())
                .setTimeout(timeOut)
                .setUnit(unit);
        cache.put(key, cacheValue);
        return true;
    }

    @Override
    public V getCache(K key) {
        CacheValue cacheValue = (CacheValue) cache.getIfPresent(key);

        if (cacheValue != null) {
            Long timeout = cacheValue.getTimeout();
            //如果为-1表示永生
            if (timeout == -1)
                return (V) cacheValue.getValue();

            //判断当前是否超时
            if (cacheValue.getCreateTime() + cacheValue.getUnit().toMillis(timeout) < System.currentTimeMillis()) {
                //已经超时
                deleteCache(key);
            } else {
                //未超时
                return (V) cacheValue.getValue();
            }
        }
        return null;
    }

    @Override
    public boolean deleteCache(K key) {
        cache.invalidate(key);
        return true;
    }

    /**
     * 缓存对象
     * @param <V>
     */
    @Data
    @Accessors(chain = true)
    private static class CacheValue<V> {
        //缓存的值
        V value;
        //超时时间
        Long timeout;
        //单位
        TimeUnit unit;
        //创建时间
        Long createTime;
    }
}
