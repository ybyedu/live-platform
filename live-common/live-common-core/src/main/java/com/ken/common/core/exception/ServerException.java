package com.ken.common.core.exception;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 自定义异常
 */
@Data
@Accessors(chain = true)
public class ServerException extends RuntimeException {

    /**
     * 异常码
     */
    private Integer code;

    /**
     * 异常信息
     */
    private String msg;
}
