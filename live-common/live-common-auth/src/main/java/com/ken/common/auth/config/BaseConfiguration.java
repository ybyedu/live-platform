package com.ken.common.auth.config;

import com.ken.common.auth.exception.AccessExceptionHandler;
import com.ken.common.auth.interception.AuthInterception;
import com.ken.common.auth.permission.AuthPermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class BaseConfiguration {

    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    @Bean
    @ConditionalOnMissingBean
    public TokenStore getTokenStore(){
        return new RedisTokenStore(redisConnectionFactory);
    }

    @Bean
    @ConditionalOnMissingBean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

    @Bean
    @ConditionalOnMissingBean({WebSecurityConfigurerAdapter.class, WebSecurityConfiguration.class})
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
    public WebSecurityConfiguration getWebSecurityConfiguration(){
        return new WebSecurityConfiguration();
    }

    /**
     * 声明身份认证拦截器
     * @return
     */
    @Bean
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
    public AuthInterception getAuthInterception(){
        return new AuthInterception();
    }

    /**
     * 声明权限验证器
     * @return
     */
    @Bean
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
    public AuthPermissionEvaluator getAuthPermissionEvaluator(){
        return new AuthPermissionEvaluator();
    }

    /**
     * 权限异常统一处理
     * @return
     */
    @Bean
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
    public AccessExceptionHandler getAccessExceptionHandler(){
        return new AccessExceptionHandler();
    }

//    /**
//     * 声明权限异常处理器
//     * @return
//     */
//    @Bean
//    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
//    public AccessDeniedHandler getAccessDeniedHandler(){
//        return new MyAccessDeniedHandler();
//    }
//
//    /**
//     * 声明认证异常处理器
//     * @return
//     */
//    @Bean
//    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
//    public AuthenticationEntryPoint getAuthenticationEntryPoint(){
//        return new MyAuthenticationEntryPoint();
//    }
}
