package com.ken.common.auth.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ken.entity.base.protocol.R;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 身份认证异常处理 - 弃用
 */
@Deprecated
public class MyAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        //设置状态码
        httpServletResponse.setStatus(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED.value());
        //设置响应头
        httpServletResponse.addHeader("Content-Type", "application/json;charset=utf-8");
        //设置响应体
        PrintWriter writer = httpServletResponse.getWriter();
        R r = new R(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED.value(), "身份认证失败", null);
        ObjectMapper objectMapper = new ObjectMapper();
        String result = objectMapper.writeValueAsString(r);
        writer.println(result);
        writer.flush();
        writer.close();
    }
}
