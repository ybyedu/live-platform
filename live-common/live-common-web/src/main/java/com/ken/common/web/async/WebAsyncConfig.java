package com.ken.common.web.async;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * 自定义的异步处理线程池
 */
public class WebAsyncConfig {

    @Autowired
    private AsyncThreadsConfiguration asyncThreadsConfiguration;

    @Bean
    @Primary
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //核心线程数 推荐设置为 CPU核心数
        executor.setCorePoolSize(asyncThreadsConfiguration.getCoreNum());
        //线程数最大值 推荐设置为CPU核心数 * 2
        executor.setMaxPoolSize(asyncThreadsConfiguration.getMaxNum());
        //线程的空闲时间，超过maxPoolSize的线程，达到空闲时间后会被回收
        executor.setKeepAliveSeconds(asyncThreadsConfiguration.getKeepAliveSeconds());
        //是否允许回收核心线程
        executor.setAllowCoreThreadTimeOut(asyncThreadsConfiguration.getSetAllowCoreThreadTimeOut());
        //设置拒绝策略，选择采用当前线程执行被拒绝的任务
        executor.setRejectedExecutionHandler(asyncThreadsConfiguration.getRejectHandler().getRejectedExecutionHandler());
        //设置线程名称的前缀
        executor.setThreadNamePrefix(asyncThreadsConfiguration.getPrefixName());
        //初始化线程池
        executor.initialize();
        return executor;
    }
}
