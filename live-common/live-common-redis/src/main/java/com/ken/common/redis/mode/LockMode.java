package com.ken.common.redis.mode;

public enum LockMode {
    AUTO, //自动加锁
    MANUAL;//手动加锁
}
