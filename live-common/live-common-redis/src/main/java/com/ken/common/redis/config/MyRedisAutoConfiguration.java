package com.ken.common.redis.config;

import com.ken.common.redis.aop.RLockAop;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyRedisAutoConfiguration {

    @Bean
    public RLockAop getRLockAop(){
        return new RLockAop();
    }
}
