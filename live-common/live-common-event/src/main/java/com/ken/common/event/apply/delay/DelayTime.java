package com.ken.common.event.apply.delay;

import java.util.concurrent.TimeUnit;

/**
 * 延迟消息的延迟时间枚举
 */
public enum DelayTime {

    SECOND_5(5, TimeUnit.SECONDS), //5秒
    SECOND_30(30, TimeUnit.SECONDS), //30秒
    MINUTE_1(1, TimeUnit.MINUTES), //1分钟
    MINUTE_30(30, TimeUnit.MINUTES), //30分钟
    HOUR_1(1, TimeUnit.HOURS), //1小时
    HOUR_10(10, TimeUnit.HOURS), //10小时
    DAY_1(1, TimeUnit.DAYS); //1天


    public int time;
    public TimeUnit unit;

    DelayTime(int time, TimeUnit unit) {
        this.time = time;
        this.unit = unit;
    }

    /**
     * 返回一个时间名称
     * @return
     */
    public String getName(){
        return time + unit.name();
    }
}
