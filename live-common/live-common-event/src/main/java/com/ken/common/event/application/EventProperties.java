package com.ken.common.event.application;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "kenplugin.event")
public class EventProperties {
    EventMq eventMq;
}
