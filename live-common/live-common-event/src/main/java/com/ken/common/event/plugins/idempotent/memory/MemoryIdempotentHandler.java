package com.ken.common.event.plugins.idempotent.memory;

import com.ken.common.event.framework.message.EventMessage;
import com.ken.common.event.framework.processor.idempotent.handler.IdempotentHandler;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * 基于内存的幂等处理器
 */
@Component
public class MemoryIdempotentHandler implements IdempotentHandler {

    private Set<String> idSet = new HashSet<>();

    @Override
    public boolean isIdempotent(EventMessage eventMessage) {
        System.out.println("还是内存幂等过滤器？");
        //获得消息ID
        String msgId = eventMessage.getMsgId();
        if (!idSet.contains(msgId)) {
            //不存在
            idSet.add(msgId);
            return true;
        }
        return false;
    }
}
