package com.ken.common.event.apply.handle;

import com.ken.common.event.framework.message.EventMessage;

/**
 * 消息的处理模板接口 - 需要开发者去实现
 * @param <T>
 */
public interface EventHandler<T> {

    void eventHandler(T t, EventMessage<T> eventMessage);
}