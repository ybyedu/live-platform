package com.ken.common.event.framework.handle;


import com.ken.common.event.framework.message.EventMessage;

/**
 * 消息处理器 - 架构内部的消息处理接口
 */
public interface MsgHandler {

    void msgHandler(String eventTypeStr, EventMessage eventMsg);
}
