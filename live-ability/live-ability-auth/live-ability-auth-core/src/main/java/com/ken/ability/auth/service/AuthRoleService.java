package com.ken.ability.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ken.entity.auth.AuthRole;

/**
 * 角色表(AuthRole)表服务接口
 *
 * @author makejava
 * @since 2021-09-12 12:29:32
 */
public interface AuthRoleService extends IService<AuthRole> {

}
