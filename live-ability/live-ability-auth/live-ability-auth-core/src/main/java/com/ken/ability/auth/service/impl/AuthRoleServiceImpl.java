package com.ken.ability.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ken.ability.auth.service.AuthRoleService;
import com.ken.entity.auth.AuthRole;
import com.ken.mapper.auth.dao.AuthRoleDao;
import org.springframework.stereotype.Service;

/**
 * 角色表(AuthRole)表服务实现类
 *
 * @author makejava
 * @since 2021-09-12 12:29:32
 */
@Service
public class AuthRoleServiceImpl extends ServiceImpl<AuthRoleDao, AuthRole> implements AuthRoleService {

}
