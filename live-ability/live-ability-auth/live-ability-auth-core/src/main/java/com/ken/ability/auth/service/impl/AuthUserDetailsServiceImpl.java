package com.ken.ability.auth.service.impl;

import com.ken.ability.auth.protocol.security.AuthUserDetails;
import com.ken.ability.auth.service.AuthUserService;
import com.ken.common.cache.annotation.CacheGet;
import com.ken.common.core.utils.BeanCopyUtils;
import com.ken.entity.auth.AuthUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Security用户信息查询的业务实现
 */
@Service
@Slf4j
public class AuthUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AuthUserService authUserService;

    @Override
    @CacheGet(key = "'auth-user-' + #username")
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AuthUser authUser = authUserService.queryAuthUserByUserName(username);
        //Security的身份认证对象
        AuthUserDetails authUserDetails = BeanCopyUtils.copyBean(authUser, AuthUserDetails.class);
        log.debug("[Auth-User] - 根据用户名获得用户信息 - {}", authUserDetails);
        return authUserDetails;
    }

}
