package com.ken.ability.auth.protocol.security;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Slf4j
public class AuthUserDetails implements UserDetails {
    //主键
    private Long id;
    //用户名
    private String username;
    //密码
    private String password;
    //昵称
    private String nickname;
    //头像
    private String headerImage;
    //邮箱
    private String email;
    //手机
    private String phone;

    /**
     * 角色列表
     */
    private List<AuthRoleDetails> roles;

    /**
     * 权限列表
     */
    private List<AuthPowerDetails> powers;


    /**
     * 返回用户角色列表
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (!CollectionUtils.isEmpty(roles)) {
            return roles.stream().filter(role -> role.getId() != null)
                    .collect(Collectors.toSet());
        }
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
