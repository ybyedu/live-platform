package com.ken.ability.search.application;

import com.ken.ability.search.service.ISysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ServerStartApplication implements CommandLineRunner {

    @Autowired
    private ISysLogService sysLogService;

    @Override
    public void run(String... args) throws Exception {
        if (sysLogService.createIndex()){
            sysLogService.createMapping();
        }
    }
}
