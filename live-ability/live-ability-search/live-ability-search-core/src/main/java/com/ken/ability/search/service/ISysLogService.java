package com.ken.ability.search.service;

import com.ken.data.entity.syslog.SysLog;
import com.ken.data.entity.syslog.qo.SysLogQo;

import java.util.List;

public interface ISysLogService {

    boolean createIndex();

    boolean createMapping();

    boolean insertSysLog(SysLog sysLog);

    List<SysLog> querySysLog(SysLogQo sysLogQo);
}
