package com.ken.ability.search.test.application;

import com.ken.ability.search.application.SearchApplication;
import com.ken.ability.search.service.ISysLogService;
import com.ken.data.entity.syslog.SysLog;
import com.ken.data.entity.syslog.qo.SysLogQo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchApplication.class)
public class SearchTestApplication {

    @Autowired
    private ISysLogService sysLogService;

    @Test
    public void test(){
        //查询对象
        SysLogQo sysLogQo = new SysLogQo();
        List<SysLog> sysLogs = sysLogService.querySysLog(sysLogQo);
        System.out.println(sysLogs);
    }
}
