package com.ken.ability.search.protocol.input;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class SysLogInput implements Serializable {

    /**
     * 主键
     */
    private Long id;

    /**
     * 应用名称
     */
    @NotNull(message = "应用名称不能为null")
    private String appName;

    /**
     * 类名称
     */
    private String className;

    /**
     * 方法名
     */
    private String method;

    /**
     * 访问时间
     */
    private Date createTime;

    /**
     * 访问用户名
     */
    private String username;

    /**
     * 请求参数
     */
    private Map<String, Object[]> params;

    /**
     * 返回结果
     */
    private String result;

    /**
     * 异常信息
     */
    private String exception;
}
